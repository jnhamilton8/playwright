class OrderDetailsPage {

    constructor(page) {
        this.page = page;
        this.orderId = page.locator(".col-text");
    }

    returnOrderId() {
        return this.orderId.textContent();
    }
}

module.exports = { OrderDetailsPage };