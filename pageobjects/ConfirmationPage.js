class ConfirmationPage {
    constructor(page) {
        this.page = page;
        this.orderConfirmation = page.locator(" .hero-primary");
        this.orderId = page.locator(".em-spacer-1 .ng-star-inserted");
    }

    async returnOrderId() {
        return this.orderId.textContent();
    }

}

module.exports = { ConfirmationPage };