const { LoginPage } = require('../pageobjects/LoginPage');
const { DashboardPage } = require('../pageobjects/DashboardPage');
const { CheckoutPage } = require('../pageobjects/CheckoutPage');
const { CartPage } = require('../pageobjects/CartPage');
const { ConfirmationPage } = require('../pageobjects/ConfirmationPage');
const { MyOrdersPage } = require('../pageobjects/MyOrdersPage');
const { OrderDetailsPage } = require('../pageobjects/OrderDetailsPage');

class POManager {
    constructor(page) {
        this.page = page;
        this.loginPage = new LoginPage(this.page);
        this.dashboardPage = new DashboardPage(this.page);
        this.checkoutPage = new CheckoutPage(this.page);
        this.cartPage = new CartPage(this.page);
        this.confirmationPage = new ConfirmationPage(this.page);
        this.myOrdersPage = new MyOrdersPage(this.page);
        this.orderDetailsPage = new OrderDetailsPage(this.page);
    }

    getLoginPage() {
        return this.loginPage;
    }

    getDashboardPage() {
        return this.dashboardPage;
    }

    getCheckoutPage() {
        return this.checkoutPage;
    }

    getCartPage() {
        return this.cartPage;
    }

    getConfirmationPage() {
        return this.confirmationPage;
    }

    getMyOrdersPage() {
        return this.myOrdersPage;
    }

    getOrderDetailsPage() {
        return this.orderDetailsPage;
    }
}

module.exports = { POManager };