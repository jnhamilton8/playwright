class CartPage {

    constructor(page) {
        this.page = page;
        this.cartItemBody = page.locator("div li");
        this.checkoutButton = page.locator("text='Checkout'");
    }

    async waitForFirstItemInPageToLoad() {
        await this.cartItemBody.first().waitFor();
    }

    async isProductVisible(productName) {
        return await this.getProductLocator(productName).isVisible();
    }

    async clickCheckoutButton() {
        this.checkoutButton.click();
    }

    getProductLocator(productName) {
        return this.page.locator(`h3:has-text('${productName}')`)

    }
}

module.exports = { CartPage };