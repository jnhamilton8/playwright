const { expect } = require('@playwright/test');

class DashboardPage {
    constructor(page) {
        this.page = page;
        this.products = page.locator(".card-body");
        this.productsText = page.locator(".card-body b");
        this.cart = page.locator("[routerlink*='cart']");
    }

    async searchProductAddCart(productName) {
        const productTitles = await this.productsText.allTextContents();
        expect(productTitles.length).toBeGreaterThanOrEqual(2);

        const count = await this.products.count();
        for (let i = 0; i < count; ++i) {
            if (await this.products.nth(i).locator("b").textContent() === productName) {
                await this.products.nth(i).locator("text= Add To Cart").click();
                break;
            } else {
                console.log("We didn't click on the add to cart button, halting test");
            }
        }
    }

    async navigateToCart() {
        await this.cart.click();
    }
}

module.exports = {DashboardPage};