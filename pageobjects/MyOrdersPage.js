class MyOrdersPage {
    constructor(page) {
        this.page = page;
        this.myOrdersButton = page.locator("button[routerlink*='myorders']");
        this.ordersBody = page.locator("tbody");
        this.orderRows = page.locator("tbody tr");
    }

    async clickMyOrdersButton() {
        await this.myOrdersButton.click();
    }

    async returnOrderRowCount() {
        return await this.orderRows.count();
    }
}

module.exports = { MyOrdersPage };