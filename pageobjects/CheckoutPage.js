class CheckoutPage {
    constructor(page) {
        this.page = page;
        this.country = page.locator("[placeholder*='Country']");
        this.countryDropdown = page.locator(".ta-results");
        this.countryOptions = page.locator("button");
        this.email = page.locator("div.user__name input.input[type='text']");
        this.proceedButton = page.locator(".action__submit");
    }

    async searchForAndSelectCountry(country, delay) {
        await this.country.type(country, { delay: delay });
        await this.countryDropdown.waitFor();

        const optionsCount = await this.countryOptions.count();

        for (let i = 0; i < optionsCount; ++i) {
            const text = await this.countryOptions.nth(i).textContent();
            if (text.trim() === country) {
                await this.countryOptions.nth(i).click();
                break;
            }
        }
    }

    async clickProceedButton() {
        await this.proceedButton.click();
    }
}

module.exports = { CheckoutPage };