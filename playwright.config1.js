// @ts-check
const { devices } = require('@playwright/test');

/**
 * @see https://playwright.dev/docs/test-configuration
 * @type {import('@playwright/test').PlaywrightTestConfig}
 */
const config = {
  workers: 6, //setting the number of parallel instances e.g test files to run at once, or set to 1 to disable 
  testDir: './tests',
  /* Maximum time one test can run for. */
  timeout: 10 * 1000,
  expect: {
    /**
     * Maximum time expect() should wait for the condition to be met.
     * For example in `await expect(locator).toHaveText();`
     */
    timeout: 3000
  },

  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  /*To run the below, add the --config playwright.config1.js --project=chrome to the end of the cmd on the cmd line  */
  reporter: 'html',
  projects: [
    {
      name : 'firefox execution',
      use: {
        browserName: 'firefox',
        headless: true,
        screenshot: 'on',
        trace: 'retain-on-failure'
      }
    },
    {
      name : 'chrome execution',
      use: {
        browserName: 'chromium',
        headless: true,
        screenshot: 'on',
        trace: 'retain-on-failure'
      }
    }
  ]
  /* Shared settings for all the projects below. See https://playwright.dev/docs/api/class-testoptions. */

};

module.exports = config;
