const { test, request } = require('@playwright/test');
const { APIUtils } = require('../utils/APIUtils')
const loginPayload = { userEmail: "jnhamilton8@yahoo.co.uk", userPassword: "Qatester123!" };
const orderPayload = { orders: [{ country: "Cuba", productOrderedId: "6262e990e26b7e1a10e89bfa" }] }
let response;

test.beforeAll(async () => {
    const apiContext = await request.newContext();
    const apiUtils = new APIUtils(apiContext, loginPayload);
    response = await apiUtils.createOrder(orderPayload);
});

test('@API Verify placed orders appear correctly on confirmation page', async ({ page }) => {
    await page.addInitScript(value => {
        window.localStorage.setItem('token', value);
    }, response.token);

    await page.goto("https://rahulshettyacademy.com/client/");

    await page.locator("button[routerlink*='myorders']").click();
    await page.route("https://rahulshettyacademy.com/api/ecom/order/get-orders-details?id=6262e990e26b7e1a10e89bfa",
        async route => await route.continue({ url: 'https://rahulshettyacademy.com/api/ecom/order/get-orders-details?id=63a9ae3b03841e9c9a60adcb' })) //different product

    await page.locator("button:has-text('View')").first().click();  //click on the first product in the list (would need to delete all there as a pre req of use a more reliable locator here in real life)
});