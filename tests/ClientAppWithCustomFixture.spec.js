const { expect } = require('@playwright/test');
const { customtest } = require('../utils/test-base');
const { POManager } = require('../pageobjects/POManager');

customtest(`Client App place order using custom fixture`, async ({ page, testDataForOrder }) => {
    const poManager = new POManager(page);
    const loginPage = poManager.getLoginPage();
    const dashboardPage = poManager.getDashboardPage();
    const cartPage = poManager.getCartPage();

    await loginPage.goToLoginPage();
    await loginPage.login(testDataForOrder.username, testDataForOrder.password);
    await page.waitForLoadState('networkidle'); //waits for all network calls to be successfully made

    await dashboardPage.searchProductAddCart(testDataForOrder.productName);
    await dashboardPage.navigateToCart()

    await cartPage.waitForFirstItemInPageToLoad();
    const isProductVisible = cartPage.isProductVisible(testDataForOrder.productName)
    expect(isProductVisible).toBeTruthy();
    await cartPage.clickCheckoutButton();
});