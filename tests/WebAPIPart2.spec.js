const { test, expect } = require('@playwright/test');
let webContext;

test.beforeAll(async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();

    await page.goto("https://rahulshettyacademy.com/client");
    await page.locator("#userEmail").type("anshika@gmail.com");
    await page.locator("#userPassword").type("Iamking@000")
    await page.locator("[value='Login']").click();
    await page.waitForLoadState('networkidle');

    await context.storageState({ path: 'state.json' }); //stores the storage data into this file
    webContext = await browser.newContext({ storageState: 'state.json' }); //passes the saved state to the new context
});

test('Verify titles of products on client site with log in in beforeAll', async () => {
    const productName = 'zara coat 3';
    const page = await webContext.newPage(); //using all the injected storage from the beforeAll
    await page.goto("https://rahulshettyacademy.com/client");

    const products = page.locator(".card-body");
    const email = "anshika@gmail.com";
    
    const productTitles = await page.locator(".card-body b").allTextContents();
    expect(productTitles.length).toBeGreaterThanOrEqual(2);

    const count = await products.count();
    for (let i = 0; i < count; ++i) {
        if (await products.nth(i).locator("b").textContent() === productName) {
            await products.nth(i).locator("text= Add To Cart").click();
            break;
        } else {
            throw new AssertionError("We didn't click on the add to cart button, halting test");
        }
    }

    await page.locator("[routerlink*='cart']").click();
    await page.locator("div li").first().waitFor();  //waits until this locator shows up on the page
    const isProductVisible = await page.locator("h3:has-text('Zara Coat 3')").isVisible();
    expect(isProductVisible).toBeTruthy();

    await page.locator("text='Checkout'").click();
    await page.locator("[placeholder*='Country']").type("ind", { delay: 100 });
    const dropdown = page.locator(".ta-results");
    await dropdown.waitFor();
    const optionsCount = await dropdown.locator("button").count();

    for (let i = 0; i < optionsCount; ++i) {
        const text = await dropdown.locator("button").nth(i).textContent();
        if (text.trim() === "India") {
            await dropdown.locator("button").nth(i).click();
            break;
        }
    }

    await expect(page.locator("div.user__name input.input[type='text']")).toHaveValue(email);
    await page.locator(".action__submit").click();
    await expect(page.locator(" .hero-primary")).toHaveText("Thankyou for the order.");
    const orderId = await page.locator(".em-spacer-1 .ng-star-inserted").textContent();

    await page.locator("button[routerlink*='myorders']").click();
    await page.locator("tbody").waitFor();
    const rows = page.locator("tbody tr");

    for (let i = 0; i < await rows.count(); ++i) {
        const rowOrderId = await rows.nth(i).locator("th").textContent();
        if (orderId.includes(rowOrderId)) {
            await rows.nth(i).locator("button").first().click();
            break;
        }
    }

    const orderIdInDetailsPage = await page.locator(".col-text").textContent();
    expect(orderId).toContain(orderIdInDetailsPage);
});

test('Print titles only to see benefit of logging in in beforeAll', async () => {
    const page = await webContext.newPage(); //using all the injected storage from the beforeAll
    await page.goto("https://rahulshettyacademy.com/client");
    await page.waitForLoadState('networkidle');

    const productTitles = await page.locator(".card-body b").allTextContents();
    expect(productTitles.length).toBeGreaterThanOrEqual(2);
})