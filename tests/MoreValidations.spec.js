const { test, expect } = require('@playwright/test');

test.describe.configure({mode: 'parallel'}); //tests will run in parallel
//test.describe.configure({mode: 'serial'}); //treat tests as dependent on each other, if one fails, will skip the others
test('@Web Page navigation, hide and view elements, and popup validations', async ({ page }) => { //uses tag web
    await page.goto("https://rahulshettyacademy.com/AutomationPractice/");
    //await page.goto("http://google.com");
    //await page.goBack();
    //await page.goForward()

    await expect(page.locator("#displayed-text")).toBeVisible();
    await page.locator("#hide-textbox").click();
    await expect(page.locator("#displayed-text")).toBeHidden();

    page.on("dialog", dialog => dialog.accept());  //will click OK on the pop up dialog in the line below
    await page.locator("#confirmbtn").click();
    await page.locator("#mousehover").hover();

    const framesPage = page.frameLocator("#courses-iframe"); //switch to iframe
    await framesPage.locator("li a[href*='lifetime-access']:visible").click(); //focus only on the visible locator, not the additional hidden one
    const textCheck = await framesPage.locator(".text h2").textContent();
    const numSubscribers = textCheck.split(" ")[1];
    expect(numSubscribers.length).toBeGreaterThan(1);
});

test('Screenshots', async ({ page }) => { 
    await page.goto("https://rahulshettyacademy.com/AutomationPractice/");
    await expect(page.locator("#displayed-text")).toBeVisible();
    await page.locator('#displayed-text').screenshot({path: 'partialScreenshot.png'});  //partial screenshot
    await page.locator("#hide-textbox").click();
    await page.screenshot({path: 'screenshot.png'}); //whole page screenshot
    await expect(page.locator("#displayed-text")).toBeHidden();
});

test('Visual comparison', async ({ page }) => { 
    await page.goto("https://flightware.com/");
    expect(await page.screenshot()).toMatchSnapshot('landing.png');
   
});