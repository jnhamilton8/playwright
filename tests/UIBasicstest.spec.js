const { test, expect } = require('@playwright/test');

test('Browser Context Playwright test', async ({ browser }) => {
    // can set your own context if wanting to pass in browser settings e.g. cookies etc.
    const context = await browser.newContext();
    const page = await context.newPage();

    page.on('request', request => console.log(request.url())); //listener which is invoked when event is invoked
    page.on('response', response => console.log(response.url(), response.status())); 

    const username = page.locator('#username');
    const password = page.locator("[type='password']")
    const signIn = page.locator("#signInBtn");
    const cardTitles = page.locator(".card-body a");

    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");

    await username.type("rahulshetty");
    await password.type("learning");
    await signIn.click();
    const alertText = await page.locator("[style*='block']").textContent();
    expect(alertText).toBe("Incorrect username/password.");

    await username.fill(""); //wipes out existing content
    await username.fill("rahulshettyacademy");

    await Promise.all([   //tells Playwright to wait for the both the below steps to complete before moving on
        page.waitForNavigation(),
        signIn.click()
    ]);

    const cardTitle1 = await cardTitles.first().textContent();
    const cardTitle2 = await cardTitles.nth(1).textContent();
    const cardTitlesText = await cardTitles.allTextContents();
    expect(cardTitle1).toBe("iphone X");
    expect(cardTitle2).toBe("Samsung Note 8");
    expect(cardTitlesText).toStrictEqual(["iphone X", "Samsung Note 8", "Nokia Edge", "Blackberry"]);

    await context.close();
});

test('Page Playwright test', async ({ page }) => { // uses page fixture to get context and page for you using default settings
    await page.goto("https://www.google.com");
    await page.title();
    await expect(page).toHaveTitle("Google");
});

test('UI Controls test', async ({ page }) => {
    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    const dropdown = page.locator('select.form-control');
    const radioButtonUser = page.locator(".radiotextsty");
    const terms = page.locator("#terms");
    const documentLink = page.locator("[href*='documents-request']")

    dropdown.selectOption("consult");
    await radioButtonUser.last().click();
    await page.locator("#okayBtn").click();
    await expect(radioButtonUser.last()).toBeChecked();
    await terms.click();
    await expect(terms).toBeChecked();
    await terms.uncheck();
    expect(await terms.isChecked()).toBeFalsy();
    await expect(documentLink).toHaveAttribute("class", "blinkingText");
});

test('Child windows handling test', async ({ browser }) => {
    const context = await browser.newContext();
    const page = await context.newPage();
    const username = page.locator('#username');

    await page.goto("https://rahulshettyacademy.com/loginpagePractise/");
    const documentLink = page.locator("[href*='documents-request']")

    const [newPage] = await Promise.all([   //waits for the new tab to open and captures it in newPage
        context.waitForEvent('page'),
        documentLink.click()
    ]);
    const text = await newPage.locator(".red").textContent();
    const textEmailDomain = text.split("@")[1].split(" ")[0];
    expect(textEmailDomain).toBe("rahulshettyacademy.com");

    await username.type(textEmailDomain);  //enter into the first tab - parent page
    const inputValue = await username.inputValue();
    expect(inputValue).toBe(textEmailDomain);
});