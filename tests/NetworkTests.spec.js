const { test, expect, request } = require('@playwright/test');
const { APIUtils } = require('../utils/APIUtils')
const loginPayload = { userEmail: "anshika@gmail.com", userPassword: "Iamking@000" };
const orderPayload = { orders: [{ country: "India", productOrderedId: "6262e95ae26b7e1a10e89bf0" }] }
const fakePayloadOrders = { data: [], message: "No Orders" }
let response;

test.beforeAll(async () => {
    const apiContext = await request.newContext();
    const apiUtils = new APIUtils(apiContext, loginPayload);
    response = await apiUtils.createOrder(orderPayload);
});

test('Verify no orders to show message appears on confirmation page when user has no orders', async ({ page }) => {
    await page.addInitScript(value => {
        window.localStorage.setItem('token', value);
    }, response.token);

    await page.goto("https://rahulshettyacademy.com/client/");

    await page.route("https://rahulshettyacademy.com/api/ecom/order/get-orders-for-customer/620c7bf148767f1f1215d2ca",
        async route => {
            const response = await page.request.fetch(route.request());  //fetch the response to the URL passed in as first param
            let body = JSON.stringify(fakePayloadOrders);
            route.fulfill({ //sends response to browser
                response,
                body, //overwrites existing body with the one supplied
            });
        });

    await page.locator("button[routerlink*='myorders']").click();
    await page.waitForResponse("https://rahulshettyacademy.com/api/ecom/order/get-orders-for-customer/620c7bf148767f1f1215d2ca");
    const noOrdersMessage = await page.locator(".mt-4").textContent();
    expect(noOrdersMessage.trim()).toBe("You have No Orders to show at this time. Please Visit Back Us")
});