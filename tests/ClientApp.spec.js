const { test, expect } = require('@playwright/test');
const { POManager } = require('../pageobjects/POManager');
const dataset = JSON.parse(JSON.stringify(require('../utils/placeOrderTestData.json')));

for (const data of dataset) { //iterate through the data in placeOrderTestData file
    test(`@Web Client App place order for ${data.productName}`, async ({ page }) => { //uses tag Web
        const poManager = new POManager(page);
        const loginPage = poManager.getLoginPage();
        const dashboardPage = poManager.getDashboardPage();
        const checkoutPage = poManager.getCheckoutPage();
        const cartPage = poManager.getCartPage();
        const confirmationPage = poManager.getConfirmationPage();
        const myOrdersPage = poManager.getMyOrdersPage();
        const orderDetailsPage = poManager.getOrderDetailsPage();

        await loginPage.goToLoginPage();
        await loginPage.login(data.username, data.password);
        await page.waitForLoadState('networkidle'); //waits for all network calls to be successfully made

        await dashboardPage.searchProductAddCart(data.productName);
        await dashboardPage.navigateToCart()

        await cartPage.waitForFirstItemInPageToLoad();
        const isProductVisible = cartPage.isProductVisible(data.productName)
        expect(isProductVisible).toBeTruthy();
        await cartPage.clickCheckoutButton();

        await checkoutPage.searchForAndSelectCountry("India", 100);
        await expect(checkoutPage.email).toHaveValue(data.username);
        await checkoutPage.clickProceedButton();

        await expect(confirmationPage.orderConfirmation).toHaveText("Thankyou for the order.");
        const orderId = await confirmationPage.returnOrderId()

        await myOrdersPage.clickMyOrdersButton();
        await myOrdersPage.ordersBody.waitFor();
        const rowCount = await myOrdersPage.returnOrderRowCount();

        for (let i = 0; i < await rowCount; ++i) {
            const rowOrderId = await myOrdersPage.orderRows.nth(i).locator("th").textContent();
            if (orderId.includes(rowOrderId)) {
                await myOrdersPage.orderRows.nth(i).locator("button").first().click();
                break;
            }
        }

        const orderIdInDetailsPage = await orderDetailsPage.returnOrderId();
        expect(orderId).toContain(orderIdInDetailsPage);
    })
};